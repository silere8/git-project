FROM artefact.skao.int/ska-tango-images-pytango-builder:9.3.34 AS buildenv
FROM artefact.skao.int/ska-tango-images-pytango-runtime:9.3.21 AS runtime

USER root

# RUN pip install poetry 

COPY pyproject.toml poetry.lock* ./ 
# WORKDIR /the/workdir/path
# RUN poetry lock --no-update
# RUN rm -rf /home/tango/.cache/pypoetry/virtualenvs/*

RUN poetry config virtualenvs.create false && poetry install
RUN apt update && apt install vim -y 
RUN pip install ipython
RUN pip install itango

# Setting the working directory
WORKDIR /app

# Copying code into the container
COPY . /app

USER tango 